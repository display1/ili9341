/*
 * tft.cpp
 *
 *  Created on: 7 апр. 2020 г.
 *      Author: gruffi
 */
#include "tft.h"
//-----------
namespace tft
{
    //---------Class Point---------
    Point::Point(): _x(0), _y(0) {}
    //---------------------------------------------------
    Point::Point(uint16_t x, uint16_t y): _x(x), _y(y) {}
    //-------------------------------------------------------------
    Point::Point(const Point &point): _x(point._x), _y(point._y) {}
    //---------------------------
    void Point::set_x(uint16_t x)
    {
        _x = x;
    };
    //---------------------------
    void Point::set_y(uint16_t y)
    {
        _y = y;
    };
    //----------------------------------------
    void Point::set_xy(uint16_t x, uint16_t y)
    {
        _x = x;
        _y = y;
    }
    //-----------------------
    uint16_t Point::x() const
    {
        return _x;
    }
    //-----------------------
    uint16_t Point::y() const
    {
        return _y;
    }
    //-----------------------------------------
    Point Point::operator +(const Point &point)
    {
        return Point(_x + point._x, _y + point._y);
    }
    //-----------------------------------------
    Point Point::operator -(const Point &point)
    {
        if(_x >= point._x && _y >= point._y)
            return Point(_x - point._x, _y - point._y);

        return *this;
    }
    //-------------------------------------------------
    bool operator == (const Point &p1, const Point &p2)
    {
        return ((p1._x == p2._x) && (p1._y == p2._y));
    }
    //-------------------------------------------------
    bool operator != (const Point &p1, const Point &p2)
    {
        return !((p1._x == p2._x) && (p1._y == p2._y));
    }
    //------------------------------------------------
    bool operator > (const Point &p1, const Point &p2)
    {
        return ((p1._x > p2._x) && (p1._y > p2._y));
    }
    //------------------------------------------------
    bool operator < (const Point &p1, const Point &p2)
    {
        return ((p1._x < p2._x) && (p1._y < p2._y));
    }
    //-----------Class TFT------------
    TFT::TFT(SPI_TypeDef *spi, TIM_TypeDef *tim, gpio::Gpio &dc, gpio::Gpio &cs, gpio::Gpio &rst):
        _WIDTH(240), _HEIGHT(320), _tim(tim), _gpio_dc(dc), _gpio_cs(cs), _gpio_rst(rst)
    {
        _gpio_dc.set();
        _gpio_cs.set();
        _gpio_rst.set();

        spi_init(spi);
        tim_clk_enable();
        init();
    }
    //---------------------------------------------------
    void TFT::draw_pixel(const Point &p, TypeColor color)
    {
        set_column_addr(p.x(), p.x());
        set_page_addr(p.y(), p.y());
        send_cmd(TFT_GRAM);
        send_data16(color);
    }
    //----------------------------------------------------------------------------------------------
    void TFT::draw_fill_rectangle(const Point &top_left, const Point &bottom_right, TypeColor color)
    {
        set_column_addr(top_left.x(), bottom_right.x());
        set_page_addr(top_left.y(), bottom_right.y());

        send_cmd(TFT_GRAM);

        for(uint16_t y = top_left.y(); y < bottom_right.y(); y++)
        {
            for(uint16_t x = top_left.x(); x < bottom_right.x(); x++)
            {
                send_data16(color);
            }
        }
    }
    //-----------------------------------------------------------------------------------------
    void TFT::draw_rectangle(const Point &top_left, const Point &bottom_right, TypeColor color)
    {
        uint16_t width = bottom_right.x() - top_left.x();
        uint16_t height = bottom_right.y() - top_left.y();

        draw_horizontal_line(top_left, width, color);
        draw_horizontal_line(Point(top_left.x(), bottom_right.y()), width, color);
        draw_vertical_line(top_left, height, color);
        draw_vertical_line(Point(bottom_right.x(), top_left.y()), height, color);
    }
    //--------------------------------------------------------------------
    void TFT::draw_line(const Point &p1, const Point &p2, TypeColor color)
    {
        int x1 = p1.x();
        int x2 = p2.x();
        int y1 = p1.y();
        int y2 = p2.y();
        int deltaX = abs(x2 - x1);
        int deltaY = abs(y2 - y1);
        int signX = (x1 < x2) ? 1 : -1;
        int signY = (y1 < y2) ? 1 : -1;
        int error = deltaX - deltaY;

        for (;;)
        {
            draw_pixel(Point(x1, y1), color);

            if(x1 == x2 && y1 == y2)
            break;

            int error2 = error * 2;

            if(error2 > -deltaY)
            {
                error -= deltaY;
                x1 += signX;
            }

            if(error2 < deltaX)
            {
                error += deltaX;
                y1 += signY;
            }
        }
    }
    //-------------------------------------------------------------------------------
    void TFT::draw_horizontal_line(const Point &start, uint16_t len, TypeColor color)
    {
        draw_line(start, Point(start.x() + len, start.y()), color);
    }
    //-----------------------------------------------------------------------------
    void TFT::draw_vertical_line(const Point &start, uint16_t len, TypeColor color)
    {
        draw_line(start, Point(start.x(), start.y() + len), color);
    }
    //--------------------------------------------------------------------------
    void TFT::draw_circle(const Point &center, uint16_t radius, TypeColor color)
    {
        int x = 0;
        int y = radius;
        int delta = 2 - 2 * radius;
        int error = 0;

        while(y >= 0)
        {
            draw_pixel(Point(center.x() + x, center.y() + y), color);
            draw_pixel(Point(center.x() + x, center.y() - y), color);
            draw_pixel(Point(center.x() - x, center.y() + y), color);
            draw_pixel(Point(center.x() - x, center.y() - y), color);

            error = 2 * (delta + y) - 1;

            if(delta < 0 && error <= 0)
            {
                ++x;
                delta += 2 * x + 1;
                continue;
            }
            error = 2 * (delta - x) - 1;
            if(delta > 0 && error > 0)
            {
                --y;
                delta += 1 - 2 * y;
                continue;
            }
            ++x;
            delta += 2 * (x - y);
            --y;
        }
    }
    //-------------------------------------------------------------------------------
    void TFT::draw_fill_circle(const Point &center, uint16_t radius, TypeColor color)
    {
        int x = -radius, y = 0, err = 2 - 2*radius, e2;

        do
        {
            draw_vertical_line(Point(center.x() - x, center.y() - y), 2*y, color);
            draw_vertical_line(Point(center.x() + x, center.y() - y), 2*y, color);

            e2 = err;

            if (e2 <= y)
            {
                err += ++y*2 + 1;

                if (-x == y && e2 <= x)
                    e2 = 0;
            }

            if (e2 > x)
                err += ++x*2+1;
        } while (x <= 0);
    }
    //-----------------------------------------------------------------------------------------
    void TFT::draw_triangle(const Point &p1, const Point &p2, const Point &p3, TypeColor color)
    {
        draw_line(p1, p2, color);
        draw_line(p2, p3, color);
        draw_line(p3, p1, color);
    }
    //-----------------------------
    void TFT::fill(TypeColor color)
    {
        set_column_addr(0, _WIDTH - 1);
        set_page_addr(0, _HEIGHT - 1);

        send_cmd(TFT_GRAM);

        for(uint32_t i = 0; i < (uint32_t)(_WIDTH*_HEIGHT) - 1; i++)
        {
            send_data16(color);
        }

        draw_fill_rectangle(Point(0, 0), Point(_WIDTH - 1, _HEIGHT - 1), color);
    }
    //--------------
    void TFT::init()
    {
        _gpio_cs.reset();

        reset();
        send_cmd(TFT_RESET);
        delay_ms(100);

        /* Power Control A */
        send_cmd(TFT_POWERA);
        send_data8(0x39);
        send_data8(0x2C);
        send_data8(0x00);
        send_data8(0x34);
        send_data8(0x02);

        /* Power Control B */
        send_cmd(TFT_POWERB);
        send_data8(0x00);
        send_data8(0xC1);
        send_data8(0x30);

        /* Driver timing control A */
        send_cmd(TFT_DTCA);
        send_data8(0x85);
        send_data8(0x00);
        send_data8(0x78);

        /* Driver timing control B */
        send_cmd(TFT_DTCB);
        send_data8(0x00);
        send_data8(0x00);

        /* Power on Sequence control */
        send_cmd(TFT_POWER_SEQ);
        send_data8(0x64);
        send_data8(0x03);
        send_data8(0x12);
        send_data8(0x81);

        /* Pump ratio control */
        send_cmd(TFT_PRC);
        send_data8(0x20);

        /* Power Control 1 */
        send_cmd(TFT_POWER1);
        send_data8(0x10);

        /* Power Control 2 */
        send_cmd(TFT_POWER2);
        send_data8(0x10);

        /* VCOM Control 1 */
        send_cmd(TFT_VCOM1);
        send_data8(0x3E);
        send_data8(0x28);

        /* VCOM Control 2 */
        send_cmd(TFT_VCOM2);
        send_data8(0x86);

        /* VCOM Control 2 */
        set_rotation(0);

        /* Pixel Format Set */
        send_cmd(TFT_PIXEL_FORMAT);
        send_data8(0x55);    //16bit

        send_cmd(TFT_FRC);
        send_data8(0x00);
        send_data8(0x18);

        /* Display Function Control */
        send_cmd(TFT_DFC);
        send_data8(0x08);
        send_data8(0x82);
        send_data8(0x27);

        /* 3GAMMA FUNCTION DISABLE */
        send_cmd(TFT_3GAMMA_EN);
        send_data8(0x00);

        /* GAMMA CURVE SELECTED */
        send_cmd(TFT_GAMMA); //Gamma set
        send_data8(0x01);    //Gamma Curve (G2.2)

        //Positive Gamma  Correction
        send_cmd(TFT_PGAMMA);
        send_data8(0x0F);
        send_data8(0x31);
        send_data8(0x2B);
        send_data8(0x0C);
        send_data8(0x0E);
        send_data8(0x08);
        send_data8(0x4E);
        send_data8(0xF1);
        send_data8(0x37);
        send_data8(0x07);
        send_data8(0x10);
        send_data8(0x03);
        send_data8(0x0E);
        send_data8(0x09);
        send_data8(0x00);

        //Negative Gamma  Correction
        send_cmd(TFT_NGAMMA);
        send_data8(0x00);
        send_data8(0x0E);
        send_data8(0x14);
        send_data8(0x03);
        send_data8(0x11);
        send_data8(0x07);
        send_data8(0x31);
        send_data8(0xC1);
        send_data8(0x48);
        send_data8(0x08);
        send_data8(0x0F);
        send_data8(0x0C);
        send_data8(0x31);
        send_data8(0x36);
        send_data8(0x0F);

        //EXIT SLEEP
        send_cmd(TFT_SLEEP_OUT);

        //TURN ON DISPLAY
        send_cmd(TFT_DISPLAY_ON);
        send_data8(0x2C);
    }
    //---------------
    void TFT::reset()
    {
        _gpio_rst.reset();
        delay_ms(50);
        _gpio_rst.set();
        delay_ms(50);
    }
    //-----------------------------------------------------
    void TFT::set_column_addr(uint16_t start, uint16_t end)
    {
        send_cmd(TFT_COLUMN_ADDR);
        send_data8(start >> 8);
        send_data8(start&0xFF);
        send_data8(end >> 8);
        send_data8(end&0xFF);
    }
    //---------------------------------------------------
    void TFT::set_page_addr(uint16_t start, uint16_t end)
    {
        send_cmd(TFT_PAGE_ADDR);
        send_data8(start >> 8);
        send_data8(start&0xFF);
        send_data8(end >> 8);
        send_data8(end&0xFF);
    }
    //-------------------------------
    void TFT::set_rotation(uint8_t r)
    {
        send_cmd(TFT_MAC);

        switch(r)
        {
            case 0:
                send_data8(0x48);
                _WIDTH  = 240;
                _HEIGHT = 320;
                break;
            case 1:
                send_data8(0x28);
                _WIDTH  = 320;
                _HEIGHT = 240;
                break;
            case 2:
                send_data8(0x88);
                _WIDTH  = 240;
                _HEIGHT = 320;
                break;
            case 3:
                send_data8(0xE8);
                _WIDTH  = 320;
                _HEIGHT = 240;
                break;
        }
    }
    //-----------------------------
    void TFT::delay_us(uint32_t us)
    {
        if(!_tim)
            return;

        _tim->PSC = F_CPU/1000000UL - 1;
        _tim->ARR = us - 1;
        _tim->DIER &= ~TIM_DIER_UIE;
        _tim->EGR |= TIM_EGR_UG;
        _tim->SR &= ~TIM_SR_UIF;
        _tim->CR1 |= TIM_CR1_CEN;

        while((_tim->SR & TIM_SR_UIF) != TIM_SR_UIF);
        _tim->SR &= ~TIM_SR_UIF;
    }
    //-----------------------------
    void TFT::delay_ms(uint32_t ms)
    {
        if(!_tim)
            return;

        _tim->PSC = F_CPU/1000UL - 1;
        _tim->ARR = ms - 1;
        _tim->DIER &= ~TIM_DIER_UIE;
        _tim->EGR |= TIM_EGR_UG;
        _tim->SR &= ~TIM_SR_UIF;
        _tim->CR1 |= TIM_CR1_CEN;

        while((_tim->SR & TIM_SR_UIF) != TIM_SR_UIF);
        _tim->SR &= ~TIM_SR_UIF;
    }
    //-----------------------------
    void TFT::send_cmd(uint8_t cmd)
    {
        _gpio_dc.reset();
        spi_send_byte(cmd);
        _gpio_dc.set();
    }
    //--------------------------------
    void TFT::send_data8(uint8_t data)
    {
        _gpio_dc.set();
        spi_send_byte(data);
    }
    //----------------------------------
    void TFT::send_data16(uint16_t data)
    {
        _gpio_dc.set();
        spi_send_word(data);
    }
    //------------------------
    void TFT::tim_clk_enable()
    {
        if(!_tim)
            return;

        if(_tim == TIM1 && !(RCC->APB2ENR & RCC_APB2ENR_TIM1EN))
            RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
        else if(_tim == TIM2 && !(RCC->APB1ENR1 & RCC_APB1ENR1_TIM2EN))
            RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN;
        else if(_tim == TIM3 && !(RCC->APB1ENR1 & RCC_APB1ENR1_TIM3EN))
            RCC->APB1ENR1 |= RCC_APB1ENR1_TIM3EN;
        else if(_tim == TIM4 && !(RCC->APB1ENR1 & RCC_APB1ENR1_TIM4EN))
            RCC->APB1ENR1 |= RCC_APB1ENR1_TIM4EN;
        else if(_tim == TIM5 && !(RCC->APB1ENR1 & RCC_APB1ENR1_TIM5EN))
            RCC->APB1ENR1 |= RCC_APB1ENR1_TIM5EN;
        else if(_tim == TIM6 && !(RCC->APB1ENR1 & RCC_APB1ENR1_TIM6EN))
            RCC->APB1ENR1 |= RCC_APB1ENR1_TIM6EN;
        else if(_tim == TIM7 && !(RCC->APB1ENR1 & RCC_APB1ENR1_TIM7EN))
            RCC->APB1ENR1 |= RCC_APB1ENR1_TIM7EN;
        else if(_tim == TIM8 && !(RCC->APB2ENR & RCC_APB2ENR_TIM8EN))
            RCC->APB2ENR |= RCC_APB2ENR_TIM8EN;
        else if(_tim == TIM15 && !(RCC->APB2ENR & RCC_APB2ENR_TIM15EN))
            RCC->APB2ENR |= RCC_APB2ENR_TIM15EN;
        else if(_tim == TIM16 && !(RCC->APB2ENR & RCC_APB2ENR_TIM16EN))
            RCC->APB2ENR |= RCC_APB2ENR_TIM16EN;
        else if(_tim == TIM17 && !(RCC->APB2ENR & RCC_APB2ENR_TIM17EN))
            RCC->APB2ENR |= RCC_APB2ENR_TIM17EN;
    }
} /* namespace tft */
