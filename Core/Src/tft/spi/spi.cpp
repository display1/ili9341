/*
 * spi.cpp
 *
 *  Created on: 7 апр. 2020 г.
 *      Author: gruffi
 */
#include "spi.h"
//--------------------------
SPI_TypeDef *_spi = nullptr;
//------------------------------
static void spi_clk_enable(void)
{
    if(!_spi)
        return;

    if(_spi == SPI1 && !(RCC->APB2ENR & RCC_APB2ENR_SPI1EN))
    {
        RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
    }
    else if(_spi == SPI2 && !(RCC->APB1ENR1 & RCC_APB1ENR1_SPI2EN))
    {
        RCC->APB1ENR1 |= RCC_APB1ENR1_SPI2EN;
    }
    else if(_spi == SPI3 && !(RCC->APB1ENR1 & RCC_APB1ENR1_SPI3EN))
    {
        RCC->APB1ENR1 |= RCC_APB1ENR1_SPI3EN;
    }
}
//-----------------------------
void spi_init(SPI_TypeDef *spi)
{
    if(!spi)
        return;

    _spi = spi;

    spi_clk_enable();

    _spi->CR1 = ((_spi->CR1 & ~(SPI_CR1_BR | SPI_CR1_CPOL | SPI_CR1_CPHA | SPI_CR1_BIDIMODE | SPI_CR1_LSBFIRST | SPI_CR1_CRCEN)) |
                 (SPI_CR1_BR_0 | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CR1_MSTR | SPI_CR1_SPE));
}
//------------------------------
void spi_send_byte(uint8_t byte)
{
    if(!_spi)
        return;

    _spi->CR1 &= ~SPI_CR1_SPE;
    _spi->CR2 &= ~SPI_CR2_DS;
    _spi->CR1 |= SPI_CR1_SPE;

    while(!(_spi->SR & SPI_SR_TXE)) {}
    *(uint8_t*)&_spi->DR = byte;
    while((_spi->SR & SPI_SR_BSY)) {}
}
//-------------------------------
void spi_send_word(uint16_t word)
{
    if(!_spi)
        return;

    _spi->CR1 &= ~SPI_CR1_SPE;
    _spi->CR2 |= SPI_CR2_DS;
    _spi->CR1 |= SPI_CR1_SPE;

    while(!(_spi->SR & SPI_SR_TXE)) {}
    _spi->DR = word;
    while((_spi->SR & SPI_SR_BSY)) {}
}
