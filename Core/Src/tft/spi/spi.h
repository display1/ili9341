/*
 * spi.h
 *
 *  Created on: 7 апр. 2020 г.
 *      Author: gruffi
 */
#ifndef _SPI_H_
    #define _SPI_H_
    //--------------------
    #include "stm32l4xx.h"
    //------------------------------
    void spi_init(SPI_TypeDef *spi);
    void spi_send_byte(uint8_t byte);
    void spi_send_word(uint16_t word);
#endif /* _SPI_H_ */
