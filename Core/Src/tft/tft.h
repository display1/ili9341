/*
 * tft.h
 *
 *  Created on: 7 апр. 2020 г.
 *      Author: gruffi
 */
#ifndef _TFT_H_
    #define _TFT_H_
    //---------------
    #include "gpio.h"
    #include "spi.h"
    #include <cmath>
    //-----------
    namespace tft
    {
        #define F_CPU 16000000UL
        //--------------
        enum TypeCommand
        {
            TFT_RESET         = (uint8_t)0x01,
            TFT_SLEEP_OUT     = (uint8_t)0x11,
            TFT_GAMMA         = (uint8_t)0x26,
            TFT_DISPLAY_OFF   = (uint8_t)0x28,
            TFT_DISPLAY_ON    = (uint8_t)0x29,
            TFT_COLUMN_ADDR   = (uint8_t)0x2A,
            TFT_PAGE_ADDR     = (uint8_t)0x2B,
            TFT_GRAM          = (uint8_t)0x2C,
            TFT_MAC           = (uint8_t)0x36,
            TFT_PIXEL_FORMAT  = (uint8_t)0x3A,
            TFT_WDB           = (uint8_t)0x51,
            TFT_WCD           = (uint8_t)0x53,
            TFT_RGB_INTERFACE = (uint8_t)0xB0,
            TFT_FRC           = (uint8_t)0xB1,
            TFT_BPC           = (uint8_t)0xB5,
            TFT_DFC           = (uint8_t)0xB6,
            TFT_POWER1        = (uint8_t)0xC0,
            TFT_POWER2        = (uint8_t)0xC1,
            TFT_VCOM1         = (uint8_t)0xC5,
            TFT_VCOM2         = (uint8_t)0xC7,
            TFT_POWERA        = (uint8_t)0xCB,
            TFT_POWERB        = (uint8_t)0xCF,
            TFT_PGAMMA        = (uint8_t)0xE0,
            TFT_NGAMMA        = (uint8_t)0xE1,
            TFT_DTCA          = (uint8_t)0xE8,
            TFT_DTCB          = (uint8_t)0xEA,
            TFT_POWER_SEQ     = (uint8_t)0xED,
            TFT_3GAMMA_EN     = (uint8_t)0xF2,
            TFT_INTERFACE     = (uint8_t)0xF6,
            TFT_PRC           = (uint8_t)0xF7
        };
        //------------
        enum TypeColor
        {
            BLACK       = (uint16_t)0x0000,
            NAVY        = (uint16_t)0x000F,
            DARKGREEN   = (uint16_t)0x03E0,
            DARKCYAN    = (uint16_t)0x03EF,
            MAROON      = (uint16_t)0x7800,
            PURPLE      = (uint16_t)0x780F,
            OLIVE       = (uint16_t)0x7BE0,
            LIGHTGREY   = (uint16_t)0xC618,
            DARKGREY    = (uint16_t)0x7BEF,
            BLUE        = (uint16_t)0x001F,
            GREEN       = (uint16_t)0x07E0,
            CYAN        = (uint16_t)0x07FF,
            RED         = (uint16_t)0xF800,
            MAGENTA     = (uint16_t)0xF81F,
            YELLOW      = (uint16_t)0xFFE0,
            WHITE       = (uint16_t)0xFFFF,
            ORANGE      = (uint16_t)0xFD20,
            GREENYELLOW = (uint16_t)0xAFE5,
            PINK        = (uint16_t)0xF81F
        };
        //---------
        class Point
        {
            public:
                Point();
                Point(uint16_t x, uint16_t y);
                Point(const Point &point);
                void set_x(uint16_t x);
                void set_y(uint16_t y);
                void set_xy(uint16_t x, uint16_t y);
                uint16_t x() const;
                uint16_t y() const;
                Point operator+ (const Point &point);
                Point operator- (const Point &point);

                friend bool operator == (const Point &p1, const Point &p2);
                friend bool operator != (const Point &p1, const Point &p2);
                friend bool operator > (const Point &p1, const Point &p2);
                friend bool operator < (const Point &p1, const Point &p2);

            private:
                uint16_t _x;
                uint16_t _y;
        };
        //--------------------------------------------------
        bool operator == (const Point &p1, const Point &p2);
        bool operator != (const Point &p1, const Point &p2);
        bool operator > (const Point &p1, const Point &p2);
        bool operator < (const Point &p1, const Point &p2);
        //-------
        class TFT
        {
            public:
                TFT(SPI_TypeDef *spi, TIM_TypeDef *tim, gpio::Gpio &dc, gpio::Gpio &cs, gpio::Gpio &rst);
                void draw_pixel(const Point &p, TypeColor color);
                void draw_fill_rectangle(const Point &top_left, const Point &bottom_right, TypeColor color);
                void draw_rectangle(const Point &top_left, const Point &bottom_right, TypeColor color);
                void draw_line(const Point &p1, const Point &p2, TypeColor color);
                void draw_horizontal_line(const Point &start, uint16_t len, TypeColor color);
                void draw_vertical_line(const Point &start, uint16_t len, TypeColor color);
                void draw_circle(const Point &center, uint16_t radius, TypeColor color);
                void draw_fill_circle(const Point &center, uint16_t radius, TypeColor color);
                void draw_triangle(const Point &p1, const Point &p2, const Point &p3, TypeColor color);
                void fill(TypeColor color);
                void init();
                void reset();
                void set_column_addr(uint16_t start, uint16_t end);
                void set_page_addr(uint16_t start, uint16_t end);
                void set_rotation(uint8_t r);

            private:
                void delay_us(uint32_t us);
                void delay_ms(uint32_t ms);
                void send_cmd(uint8_t cmd);
                void send_data8(uint8_t data);
                void send_data16(uint16_t data);
                void tim_clk_enable();

            private:
                uint16_t     _WIDTH;
                uint16_t     _HEIGHT;
                TIM_TypeDef *_tim;
                gpio::Gpio   _gpio_dc;
                gpio::Gpio   _gpio_cs;
                gpio::Gpio   _gpio_rst;
        };
    } /* namespace tft */
#endif /* _TFT_H_ */
