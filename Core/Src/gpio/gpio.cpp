/*
 * gpio.cpp
 *
 *  Created on: Apr 6, 2020
 *      Author: gruffi
 */
#include "gpio.h"
//------------
namespace gpio
{
    Gpio::Gpio(): _port(nullptr), _pin(GPIO_PIN_0) {}
    Gpio::Gpio(GPIO_TypeDef *port, TypeGpioPin pin, TypeGpioConfig mode, TypeGpioSpeed speed, TypeGpioAlt af)
    {
        init(port, pin, mode, speed, af);
    }
    //-------------------------------------------------------------------------------------------------------------
    void Gpio::init(GPIO_TypeDef *port, TypeGpioPin pin, TypeGpioConfig mode, TypeGpioSpeed speed, TypeGpioAlt af)
    {
        if(!port)
            return;

        _port = port;
        _pin  = pin;

        clock_enable();

        uint32_t pin_pos = (_pin << 1); // pin position in registers
        uint32_t moder   = (mode >> 8)&0x00000003;
        uint32_t otyper  = (mode >> 4)&0x00000001;
        uint32_t pupdr   = (mode&0x00000003);

        _port->MODER = ((_port->MODER & ~(3UL << pin_pos)) | (moder << pin_pos));
        _port->OTYPER |= (otyper << _pin);
        _port->OSPEEDR = ((_port->OSPEEDR & ~(3UL << pin_pos)) | (speed << pin_pos));
        _port->PUPDR = ((_port->PUPDR & ~(3UL << pin_pos)) | (pupdr << pin_pos));

        if(af != GPIO_AF_NO)
        {
            uint32_t alt_pin = (_pin << 2);
            uint8_t alt_reg = (_pin > 7)?1:0;

            _port->AFR[alt_reg] = (((_port->AFR[alt_reg] & ~(0xFUL << alt_pin))) | (af << alt_pin));
        }
    }
    //----------------
    void Gpio::reset()
    {
        if(!_port)
            return;

        _port->BSRR |= ((1UL << _pin) << 16);
    }
    //--------------
    void Gpio::set()
    {
        if(!_port)
            return;

        _port->BSRR |= (1UL << _pin);
    }
    //----PRIVATE METHODS----
    void Gpio::clock_enable()
    {
        if(_port == GPIOA && !(RCC->AHB2ENR & RCC_AHB2ENR_GPIOAEN))
            RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN;
        else if(_port == GPIOB && !(RCC->AHB2ENR & RCC_AHB2ENR_GPIOBEN))
            RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
        else if(_port == GPIOC && !(RCC->AHB2ENR & RCC_AHB2ENR_GPIOCEN))
            RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN;
        else if(_port == GPIOD && !(RCC->AHB2ENR & RCC_AHB2ENR_GPIODEN))
            RCC->AHB2ENR |= RCC_AHB2ENR_GPIODEN;
        else if(_port == GPIOE && !(RCC->AHB2ENR & RCC_AHB2ENR_GPIOEEN))
            RCC->AHB2ENR |= RCC_AHB2ENR_GPIOEEN;
        else if(_port == GPIOF && !(RCC->AHB2ENR & RCC_AHB2ENR_GPIOFEN))
            RCC->AHB2ENR |= RCC_AHB2ENR_GPIOFEN;
        else if(_port == GPIOG && !(RCC->AHB2ENR & RCC_AHB2ENR_GPIOGEN))
            RCC->AHB2ENR |= RCC_AHB2ENR_GPIOGEN;
        else if(_port == GPIOH && !(RCC->AHB2ENR & RCC_AHB2ENR_GPIOHEN))
            RCC->AHB2ENR |= RCC_AHB2ENR_GPIOHEN;
    }
} /* namespace gpio */
