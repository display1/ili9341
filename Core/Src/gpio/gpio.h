/*
 * gpio.h
 *
 *  Created on: Apr 6, 2020
 *      Author: gruffi
 */
#ifndef _GPIO_H_
    #define _GPIO_H_
    //--------------------
    #include "stm32l4xx.h"
    //------------
    namespace gpio
    {
        enum TypeGpioPin
        {
            GPIO_PIN_0  = (uint32_t)0x00000000,
            GPIO_PIN_1  = (uint32_t)0x00000001,
            GPIO_PIN_2  = (uint32_t)0x00000002,
            GPIO_PIN_3  = (uint32_t)0x00000003,
            GPIO_PIN_4  = (uint32_t)0x00000004,
            GPIO_PIN_5  = (uint32_t)0x00000005,
            GPIO_PIN_6  = (uint32_t)0x00000006,
            GPIO_PIN_7  = (uint32_t)0x00000007,
            GPIO_PIN_8  = (uint32_t)0x00000008,
            GPIO_PIN_9  = (uint32_t)0x00000009,
            GPIO_PIN_10 = (uint32_t)0x0000000A,
            GPIO_PIN_11 = (uint32_t)0x0000000B,
            GPIO_PIN_12 = (uint32_t)0x0000000C,
            GPIO_PIN_13 = (uint32_t)0x0000000D,
            GPIO_PIN_14 = (uint32_t)0x0000000E,
            GPIO_PIN_15 = (uint32_t)0x0000000F
        };
        //-----------------
        enum TypeGpioConfig
        {
            GPIO_OUT_PP      = (uint32_t)0x00000100,
            GPIO_OUT_PP_PU   = (uint32_t)0x00000101,
            GPIO_OUT_PP_PD   = (uint32_t)0x00000102,
            GPIO_OUT_OD      = (uint32_t)0x00000110,
            GPIO_OUT_OD_PU   = (uint32_t)0x00000111,
            GPIO_OUT_OD_PD   = (uint32_t)0x00000112,
            GPIO_AF_PP       = (uint32_t)0x00000200,
            GPIO_AF_PP_PU    = (uint32_t)0x00000201,
            GPIO_AF_PP_PD    = (uint32_t)0x00000202,
            GPIO_AF_OD       = (uint32_t)0x00000210,
            GPIO_AF_OD_PU    = (uint32_t)0x00000211,
            GPIO_AF_OD_PD    = (uint32_t)0x00000212,
            GPIO_IN_FLOATING = (uint32_t)0x00000000,
            GPIO_IN_PU       = (uint32_t)0x00000001,
            GPIO_IN_PD       = (uint32_t)0x00000002,
            GPIO_ANALOG      = (uint32_t)0x00000300
        };
        //----------------
        enum TypeGpioSpeed
        {
            GPIO_SPEED_LOW       = (uint32_t)0x00000000,
            GPIO_SPEED_MEDIUM    = (uint32_t)0x00000001,
            GPIO_SPEED_HIGH      = (uint32_t)0x00000002,
            GPIO_SPEED_VERY_HIGH = (uint32_t)0x00000003
        };
        //--------------
        enum TypeGpioAlt
        {
            GPIO_AF_0  = (uint32_t)0x00000000,
            GPIO_AF_1  = (uint32_t)0x00000001,
            GPIO_AF_2  = (uint32_t)0x00000002,
            GPIO_AF_3  = (uint32_t)0x00000003,
            GPIO_AF_4  = (uint32_t)0x00000004,
            GPIO_AF_5  = (uint32_t)0x00000005,
            GPIO_AF_6  = (uint32_t)0x00000006,
            GPIO_AF_7  = (uint32_t)0x00000007,
            GPIO_AF_8  = (uint32_t)0x00000008,
            GPIO_AF_9  = (uint32_t)0x00000009,
            GPIO_AF_10 = (uint32_t)0x0000000A,
            GPIO_AF_11 = (uint32_t)0x0000000B,
            GPIO_AF_12 = (uint32_t)0x0000000C,
            GPIO_AF_13 = (uint32_t)0x0000000D,
            GPIO_AF_14 = (uint32_t)0x0000000E,
            GPIO_AF_15 = (uint32_t)0x0000000F,
            GPIO_AF_NO = (uint32_t)0xFFFFFFFF
        };
        //--------
        class Gpio
        {
            public:
                Gpio();
                Gpio(GPIO_TypeDef *port, TypeGpioPin pin, TypeGpioConfig mode, TypeGpioSpeed speed, TypeGpioAlt af = GPIO_AF_NO);
                void init(GPIO_TypeDef *port, TypeGpioPin pin, TypeGpioConfig mode, TypeGpioSpeed speed, TypeGpioAlt af = GPIO_AF_NO);
                void reset();
                void set();

            private:
                void clock_enable();

            private:
                GPIO_TypeDef *_port;
                TypeGpioPin   _pin;
        };
    } /* namespace gpio */
#endif /* _GPIO_H_ */
